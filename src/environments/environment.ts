// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB89LLEMlWGAHbAaMk1jfLARiAs2Qavf8k',
    authDomain: 'sofiaphotography.firebaseapp.com',
    databaseURL: 'https://sofiaphotography.firebaseio.com',
    projectId: 'sofiaphotography',
    storageBucket: 'sofiaphotography.appspot.com',
    messagingSenderId: '837782440246',
    appId: '1:837782440246:web:f7933be2392d32879f0b97'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
