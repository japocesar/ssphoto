export const photos = [
    { isInput: false, name: 'name' },
    { isInput: false, name: 'size' },
];

export const films = [
    { isInput: false, name: 'name' },
    { isInput: true, name: 'director', placeholder: 'Director', type: 'string' },
    { isInput: true, name: 'year', placeholder: new Date().getFullYear(), type: 'number' },
];